package com.example.mealupproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var mealFragment: MealFragment
    lateinit var profileFragment: ProfileFragment
    lateinit var signInFragment: SignInFragment
    lateinit var signUpFragment: SignUpFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        title=resources.getString(R.string.home)
        loadFragment(MealFragment())

        NavigationView.OnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.profileFragment-> {
                    title=resources.getString(R.string.profile)
                    loadFragment(ProfileFragment())
                }

                R.id.mealFragment-> {
                    title=resources.getString(R.string.home)
                    loadFragment(MealFragment())
                }

            }
            false

        }
    }

    private fun loadFragment(fragment: Fragment) {
        // load fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

        }

